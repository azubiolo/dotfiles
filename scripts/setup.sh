#!/bin/bash

bash symlink.sh
bash aptinstall.sh
bash snapinstall.sh
bash programs.sh

# Get all upgrades
sudo apt upgrade -y

# git completion and 
curl -L https://raw.github.com/git/git/master/contrib/completion/git-completion.bash > ~/.git-completion.bash
curl -L https://raw.github.com/git/git/master/contrib/completion/git-prompt.sh > ~/.git-prompt.bash

# See our bash changes
source ~/.bashrc