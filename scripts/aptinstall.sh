#!/bin/bash

sudo apt update

function install {
  which $1 &> /dev/null

  if [ $? -ne 0 ]; then
    echo "Installing: ${1}..."
    sudo apt install -y $1
  else
    echo "Already installed: ${1}"
  fi
}

# Basics
install git g++ build-essential qt5-qmake qt5-default qttools5-dev-tools
install chromium-browser
install curl
install exfat-utils
install file
install git
install htop
install nmap
install openvpn
install vim
install flameshot

# Image processing
install gimp
install kolourpaint

# Fun stuff
install figlet
install lolcat